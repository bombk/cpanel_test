<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('content');
            $table->text('summary')->nullable(); // An excerpt or summary of the news article
            $table->string('image')->nullable(); // Path or URL to the news image
            $table->unsignedBigInteger('catagory_id'); // Assuming a foreign key relationship with categories
            $table->unsignedBigInteger('user_id'); // Assuming a foreign key relationship with users (author of the news)
            $table->timestamps();

            // Foreign key constraints
            $table->foreign('catagory_id')->references('id')->on('catagories')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('news');
    }
};
