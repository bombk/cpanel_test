<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\CatagoriesController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\OpenAIController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/',[HomeController::class,'homepage'])->name('homepage');
Route::get('/admin',[HomeController::class,'index'])->name('adminhome');
Route::post('/contacts',[HomeController::class,'contacts'])->name('contacts');
Route::get('/contact',[HomeController::class,'contact'])->name('contact');
Route::get('/about',[HomeController::class,'about'])->name('about');
Route::get('/category',[HomeController::class,'category'])->name('category');
Route::get('/search',[HomeController::class,'search'])->name('search');
Route::get('/single_post/{id}',[HomeController::class,'singlepost'])->name('singlepost');

//news
Route::get('/news',[NewsController::class,'index'])->name('news');
Route::get('/news_list',[NewsController::class,'news_list'])->name('news_list');
Route::post('/create_news',[NewsController::class,'create'])->name('create_news');
Route::get('/edit_news/{id}',[NewsController::class,'edit'])->name('edit_news');
Route::post('/update_news/{id}',[NewsController::class,'update'])->name('update_news');
Route::get('/delete_news/{id}',[NewsController::class,'destroy'])->name('delete_news');
Route::get('/getdata',[NewsController::class,'getLatestNews'])->name(('getdata'));

//catagories
Route::get('/catagory',[CatagoriesController::class,'index'])->name('catagory');
Route::post('/create_catagory',[CatagoriesController::class,'create'])->name('create_catagory');


Route::get('/youtubeai',[OpenAIController::class,'getview'])->name('youtubeai');

Route::get('/openai',[OpenAIController::class,'index'])->name('openai');

