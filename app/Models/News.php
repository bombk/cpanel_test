<?php

namespace App\Models;
use App\Models\Categories;
use App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;
    protected $fillable = ['title', 'content', 'summary' ,'catagory_id'];
    public function catagory()
    {
        return $this->belongsTo(Catagories::class,'catagory_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id','id');
    }
}
