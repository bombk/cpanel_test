<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\News;

class HomeController extends Controller
{
    //
    public function index(){
        return view('admin.admin');
    }

    public function contacts(Request $request){
        $data=new Contact();
        $data->name=$request['name'];
        $data->email=$request['email'];
        $data->number=$request['number'];
        $data->message=$request['message'];
        $data->save();

        return redirect()->back()->with('success','Your message send successfully!');
    }

    public function homepage(){
        $news_list=News::with('catagory','user')->latest()->get();
        // return $news_list;
        $program_news=News::with('catagory','user')->where('catagory_id',1)->latest()->get();
        $system_news=News::with('catagory','user')->where('catagory_id',2)->latest()->get();
        $network_news=News::with('catagory','user')->where('catagory_id',3)->latest()->get();

        //  return $business_news;
        return view('index',compact('news_list','program_news','system_news','network_news'));
    }

    public function singlepost($id){
        $news_details=News::with('catagory')->where('id',$id)->get();
        return view('single-post',compact('news_details'));
    }


    public function about(){
        return view('about');
    }
    public function contact(){
        return view('contact');

    }
    public function category(){
        return view('category');
    }
    public function search(){
        return view('search-result');
    }

}
