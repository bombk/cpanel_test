<?php

namespace App\Http\Controllers;

use App\Models\Catagories;
use Illuminate\Http\Request;

class CatagoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        return view('admin.Categories.catagories');

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        //
        // return $request->all();

        $catagory=new Catagories();
        $catagory->code=$request['code'];
        $catagory->catagories_name=$request['name'];
        $catagory->save();
        return redirect()->back()->with('success','catagory added successfully');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //

    }

    /**
     * Display the specified resource.
     */
    public function show(Catagories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Catagories $categories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Catagories $categories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Catagories $categories)
    {
        //
    }
}
