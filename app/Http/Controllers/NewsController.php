<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use Illuminate\Http\Request;
use App\Models\Catagories;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
        $catagories=Catagories::all();
        // return $catagories;

        return view('admin.News.create_news',compact('catagories'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        //
        // return $request->image;
        $request->validate([
            'title'=>'required',
            'content'=>'required',
            'summary'=>'required',
            'image' => 'required|image|max:1024', // max 2MB
            'catagory'=>'required'
        ]);

        $imageName = $request->image->getClientOriginalName();
        $request->image->move(public_path('images/news/'.$request->catagory), $imageName);

        $news=new News();
        $news->title=$request->title;
        $news->content=$request->content;
        $news->summary=$request->summary;
        $news->image='images/news/'.$request->catagory.'/'.$imageName;
        $news->catagory_id=$request->catagory;
        $news->user_id='1';
        $news->save();
        return redirect()->back()->with('success','new news added successfully');



    }

    public function news_list(){
        $news=News::with('catagory')->get();
        return view('admin.News.list_news',compact('news'));
    }

    public function getLatestNews(){
        $data=News::find(2);
        return $data;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreNewsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        //

        $news=News::with('catagory')->where('id',$id)->get();
        // return $news;
        $catagories=Catagories::all();
        return view('admin.News.edit_news',compact('news','catagories'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData=$request->validate([
            'title'=>'required',
            'content'=>'required',
            'summary'=>'required',
            'catagory_id'=>'required'
        ]);

        $news = News::findOrFail($id);
        $news->update($validatedData);
        return redirect()->back()->with('success','data update successfully.');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        //
        News::destroy($id);
        return redirect()->back()->with('success','data deleted successfully.');
    }
}
