<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use OpenAI\Laravel\Facades\OpenAI;

class OpenAIController extends Controller
{
    //
    public function getview(){
        return view('openai.openai');

    }
    public function index(Request $request){
        $request->validate([
            'prompt'=>'required'
        ]);
        // return $request->all();

        $promt="write a description of youtube video for this title ".$request->prompt." with 500 words and write 15 tags in a list formate.Also generate fully described relevent image prompt for this.";
        $result = OpenAI::chat()->create([
            'model' => 'gpt-3.5-turbo',
            'messages' => [
                ['role' => 'user', 'content' => $promt],
            ],
        ]);
        $output=$result->choices[0]->message->content;
        return view('openai.openai',compact('output'));

        // $imagescreate=OpenAI::images()->create([
        //     'prompt' => 'a robot using laptop for programming futuristic technology',
        //     'n' => 1,
        //     'size' => '1024x720',

        // ]);
        // return ($imagescreate->data[0]->url);
        // dd($result->choices[0]->message->content['Title']);
        // if (preg_match('/\d+\.\s+(.+)/', $result->choices[0]->message->content, $matches)) {
        //     $title = $matches;
        //     print_r($matches);
        // } else {
        //     echo "Title not found.";
        // }

        // echo $result->choices[0]->message->content; // Hello! How can I assist you today?

    }
}
