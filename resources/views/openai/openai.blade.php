@extends('layouts.master')
@section('body')
<div class="card">
    <div class="card-header">
        <h1>hello</h1>
    </div>
    <div class="card-body">
        <form action="{{route('openai')}}" method="GET">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Give your youtube title </label>
                <input type="text" class="form-control" id="prompt" name="prompt"  placeholder="Eg. How to earn money online">
              </div>

              <button type="submit" class="btn btn-primary">Submit</button>

        </form>
</div>
<div>
    @if(!empty($output))
    {{$output}}
    @endif
</div>

@endsection
