@extends('admin.master.master')
@section('styles')
<link rel="stylesheet" href="//cdn.datatables.net/1.13.7/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endsection
@section('news', 'active')
@section('main-body')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Total news data</h3>
        </div>

        <div class="card-body">
            <table id="example" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>ID</th>
                        <th>Title</th>
                        <th>Content</th>
                        <th>Summary</th>
                        <th>Image</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($news as $new)
                        <tr>
                            <td> {{ $loop->iteration }}</td>
                            <td>{{ $new->id }}</td>
                            <td>{{ $new->title }}</td>
                            <td>{{ $new->content }}</td>
                            <td>{{ $new->summary }}</td>
                            <td>{{ $new->image }}</td>
                            <td>{{ $new->catagory->categories_name }}</td>
                            <td>
                                <a href="{{route('edit_news',$new->id)}}"> <i class="far fa-edit"></i></a>

                                <a href="{{route('delete_news',$new->id)}}" onclick="alert('Are you sure to delete?')"> <i class="far fa-trash"></i></a>



                            </td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>

    </div>
@endsection
@section('scripts')
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script
			  src="https://code.jquery.com/jquery-3.7.1.min.js"
			  integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo="
			  crossorigin="anonymous"></script>

<script src="//cdn.datatables.net/1.13.7/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function (){
        $('#myTable').DataTable();
    });
</script>
@endsection
