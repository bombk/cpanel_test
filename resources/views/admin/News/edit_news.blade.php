@extends('admin.master.master')
@section('news', 'active')
@section('main-body')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Add new news</h3>
        </div>
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @elseif(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        @foreach ($news as $new)
        @endforeach
        <form action="{{route('update_news',$new->id)}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="newstitle">Title</label>
                    <input type="text" class="form-control" id="title" name="title" value="{{$new->title}}" placeholder="Enter new title">
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" class="form-control" cols="30" rows="5">{{$new->content}}</textarea>
                </div>
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea name="summary" id="summary" class="form-control" cols="30" rows="3">{{$new->summary}}</textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Upload image</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="image" value="{{$new->image}}" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="catagory">Select catagory</label>
                    <select name="category_id" class="form-control" id="catagory">
                        @foreach ($catagories as $catagory )
                        <option value="{{$catagory->id}}"
                            @if($catagory->id == $new->catagory->id) selected @endif>
                            {{$catagory->categories_name}}
                        </option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </form>
    </div>

@endsection
