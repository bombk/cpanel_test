@extends('admin.master.master')
@section('news', 'active')
@section('main-body')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Add new news</h3>
        </div>
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @elseif(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <form action="{{route('create_news')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
                <div class="form-group">
                    <label for="newstitle">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter new title">
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="content" class="form-control" cols="30" rows="5"></textarea>
                </div>
                <div class="form-group">
                    <label for="summary">Summary</label>
                    <textarea name="summary" id="summary" class="form-control" cols="30" rows="3"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">Upload image</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="image" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text">Upload</span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="catagory">Select catagory</label>
                    <select name="catagory" class="form-control" id="catagory">
                        <option value="" selected>Select catagory</option>
                        @foreach ($catagories as $catagory )
                        <option value="{{$catagory->id}}">{{$catagory->catagories_name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </form>
    </div>

@endsection
