@extends('admin.master.master')
@section('master_params', 'active')
@section('main-body')
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Add new Categories</h3>
        </div>
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @elseif(session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
        @endif
        <form action="{{route('create_catagory')}}" method="POST">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="catagorycode">Catagory code</label>
                            <input type="text" class="form-control" id="catagorycode" name="code"
                                placeholder="Enter catagory code">
                        </div>
                    </div>
                    <div class="col-md-8">

                        <div class="form-group">
                            <label for="newstitle">Catagory name</label>
                            <input type="text" class="form-control" id="title" name="name"
                                placeholder="Enter catagory name">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-info">Submit</button>
            </div>
        </form>
    </div>

@endsection
