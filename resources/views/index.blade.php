@extends('layouts.master')
@section('body')
<main id="main">

    <!-- ======= Hero Slider Section ======= -->
    <section id="hero-slider" class="hero-slider">
      <div class="container-md" data-aos="fade-in">
        <div class="row">
          <div class="col-12">
            <div class="swiper sliderFeaturedPosts">
              <div class="swiper-wrapper">
                @foreach ($news_list as $key=> $news )
                @if($key <=3 )
                <div class="swiper-slide">
                  <a href="{{route('singlepost',$news->id)}}" class="img-bg d-flex align-items-end" style="background-image: url('{{$news->image}}');">
                    <div class="img-bg-inner">
                      <h2>{{$news->title}}</h2>
                      <p class="text-truncate">{{$news->content}}</p>
                    </div>
                  </a>
                </div>
                @endif
                @endforeach

              </div>
              <div class="custom-swiper-button-next">
                <span class="bi-chevron-right"></span>
              </div>
              <div class="custom-swiper-button-prev">
                <span class="bi-chevron-left"></span>
              </div>

              <div class="swiper-pagination"></div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Hero Slider Section -->

    <!-- ======= Post Grid Section ======= -->
    <section id="posts" class="posts">
      <div class="container" data-aos="fade-up">
        <div class="row g-5">
          <div class="col-lg-4">
            @foreach ($news_list as $key=> $news )
            @if($key <=1 )
            <div class="post-entry-1 lg">
              <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
              <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{ date('y-M-d', strtotime($news->created_at)) }}</span></div>
              <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
              <p class="mb-4 d-block text-truncate">{{$news->content}}</p>

              <div class="d-flex align-items-center author">
                <div class="photo"><img src="{{$news->user->image}}" alt="" class="img-fluid"></div>
                <div class="name">
                  <h3 class="m-0 p-0">{{$news->user->name}}</h3>
                </div>
              </div>
            </div>
            @endif
            @endforeach

          </div>

          <div class="col-lg-8">
            <div class="row g-5">
              <div class="col-lg-4 border-start custom-border">
                @foreach ($news_list as $key=> $news )
                @if($key <=3 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                </div>
                @endif
                @endforeach
              </div>
              <div class="col-lg-4 border-start custom-border">
                @foreach ($news_list as $key=> $news )
                @if($key > 3 && $key <=7 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                </div>
                @endif
                @endforeach


              </div>

              <!-- Trending Section -->
              <div class="col-lg-4">

                <div class="trending">
                  <h3>Trending</h3>
                  <ul class="trending-post">
                    @foreach ($news_list as $key=>$news )
                    @if($key <=5)
                    <li>
                        <a href="{{route('singlepost',$news->id)}}">
                          <span class="number">{{$loop->iteration}}</span>
                          <h3>{{$news->title}}</h3>
                          <span class="author">{{$news->user->name}}</span>
                        </a>
                      </li>
                    @endif
                    @endforeach
                  </ul>
                </div>

              </div> <!-- End Trending Section -->
            </div>
          </div>

        </div> <!-- End .row -->
      </div>
    </section> <!-- End Post Grid Section -->

    <!-- ======= Culture Category Section ======= -->
    <section class="category-section">
      <div class="container" data-aos="fade-up">

        <div class="section-header d-flex justify-content-between align-items-center mb-5">
          <h2>Software development </h2>
          <div><a href="category.html" class="more">See All Software development blogs</a></div>
        </div>

        <div class="row">
          <div class="col-md-9">
            <div class="d-lg-flex post-entry-2">
            @foreach ($program_news as $key=>$news )
            @if($key ==0)
              <a href="{{route('singlepost',$news->id)}}" class="me-4 thumbnail mb-4 mb-lg-0 d-inline-block">
                <img src="{{$news->image}}" alt="" class="img-fluid">
              </a>
              <div>
                <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                <h3><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h3>
                <p class="">{{$news->content}}</p>
                <div class="d-flex align-items-center author">
                  <div class="photo"><img src="{{$news->user->image}}" alt="" class="img-fluid"></div>
                  <div class="name">
                    <h3 class="m-0 p-0">{{$news->user->name}}</h3>
                  </div>
                </div>
              </div>

            @endif
            @endforeach
        </div>
            <div class="row">
              <div class="col-lg-4">
                @foreach ($program_news as $key=>$news )
                @if($key ==1 )
                <div class="post-entry-1 border-bottom">
                    <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                    <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                    <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                    <span class="author mb-3 d-block">{{$news->user->name}}</span>
                    <p class="mb-4 d-block">{{$news->content}}</p>
                  </div>
                @endif
                @endforeach

                @foreach ($program_news as $key=>$news )
                @if($key ==2 )
                <div class="post-entry-1">
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                </div>
                @endif
                @endforeach
              </div>
              <div class="col-lg-8">
                @foreach ($program_news as $key=>$news )
                @if($key ==3 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                  <p class="mb-4 d-block">{{$news->content}}</p>
                </div>
                @endif
                @endforeach
              </div>
            </div>
          </div>

          <div class="col-md-3">
            @foreach ($program_news as $key=>$news )
            @if($key >= 0 && $key <=5 )
            <div class="post-entry-1 border-bottom">
              <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
              <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
              <span class="author mb-3 d-block">{{$news->user->name}}</span>
            </div>
            @endif
            @endforeach
          </div>
        </div>
      </div>
    </section><!-- End Culture Category Section -->

    <!-- ======= Business Category Section ======= -->
    <section class="category-section">
      <div class="container" data-aos="fade-up">

        <div class="section-header d-flex justify-content-between align-items-center mb-5">
          <h2>System design</h2>
          <div><a href="category.html" class="more">See All System design blogs</a></div>
        </div>

        <div class="row">
          <div class="col-md-9 order-md-2">

            <div class="d-lg-flex post-entry-2">
                @foreach ($system_news as $key=>$news )
                @if($key ==0 )
              <a href="{{route('singlepost',$news->id)}}" class="me-4 thumbnail d-inline-block mb-4 mb-lg-0">
                <img src="{{$news->image}}" alt="" class="img-fluid">
              </a>
              <div>
                <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                <h3><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h3>
                <p>{{$news->content}}</p>
                <div class="d-flex align-items-center author">
                  <div class="photo"><img src="{{$news->user->image}}" alt="" class="img-fluid"></div>
                  <div class="name">
                    <h3 class="m-0 p-0">{{$news->user->name}}</h3>
                  </div>
                </div>
              </div>
              @endif
              @endforeach
            </div>

            <div class="row">
              <div class="col-lg-4">
                @foreach ($system_news as $key=>$news )
                @if($key ==1 )
                <div class="post-entry-1 border-bottom">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                  <p class="mb-4 d-block">{{$news->content}}</p>
                </div>
                @endif
                @endforeach

                @foreach ($system_news as $key=>$news )
                @if($key ==2 )
                <div class="post-entry-1">
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                </div>
                @endif
                @endforeach
              </div>
              <div class="col-lg-8">
                @foreach ($system_news as $key=>$news )
                @if($key ==3 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                  <p class="mb-4 d-block">{{$news->content}}</p>
                </div>
                @endif
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-md-3">
            @foreach ($system_news as $key=>$news )
            @if($key >= 0 && $key <=5 )
            <div class="post-entry-1 border-bottom">
              <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
              <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
              <span class="author mb-3 d-block">{{$news->user->name}}</span>
            </div>
            @endif
            @endforeach
          </div>
        </div>
      </div>
    </section><!-- End Business Category Section -->

    <!-- ======= Lifestyle Category Section ======= -->
    <section class="category-section">
      <div class="container" data-aos="fade-up">

        <div class="section-header d-flex justify-content-between align-items-center mb-5">
          <h2>Network</h2>
          <div><a href="category.html" class="more">See All Network</a></div>
        </div>

        <div class="row g-5">
          <div class="col-lg-4">
            @foreach ($network_news as $key=>$news )
            @if($key == 0 )
            <div class="post-entry-1 lg">
              <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
              <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
              <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
              <p class="mb-4 d-block">{{$news->content}}</p>

              <div class="d-flex align-items-center author">
                <div class="photo"><img src="{{$news->user->image}}" alt="" class="img-fluid"></div>
                <div class="name">
                  <h3 class="m-0 p-0">{{$news->user->name}}</h3>
                </div>
              </div>
            </div>
            @endif
            @endforeach

            @foreach ($network_news as $key=>$news )
            @if($key > 0 && $key <=2 )

            <div class="post-entry-1 border-bottom">
              <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
              <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
              <span class="author mb-3 d-block">{{$news->user->name}}</span>
            </div>
            @endif
            @endforeach

          </div>

          <div class="col-lg-8">
            <div class="row g-5">
              <div class="col-lg-4 border-start custom-border">
                @foreach ($network_news as $key=>$news )
                @if($key >= 0 && $key <=5 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                </div>
                @endif
                @endforeach
              </div>
              <div class="col-lg-4 border-start custom-border">
                @foreach ($network_news as $key=>$news )
                @if($key >= 0 && $key <=5 )
                <div class="post-entry-1">
                  <a href="{{route('singlepost',$news->id)}}"><img src="{{$news->image}}" alt="" class="img-fluid"></a>
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                </div>
                @endif
                @endforeach
              </div>
              <div class="col-lg-4">
                @foreach ($network_news as $key=>$news )
                @if($key >= 0 && $key <=5 )
                <div class="post-entry-1 border-bottom">
                  <div class="post-meta"><span class="date">{{$news->catagory->categories_name}}</span> <span class="mx-1">&bullet;</span> <span>{{date('y-M-d', strtotime($news->created_at)) }}</span></div>
                  <h2 class="mb-2"><a href="{{route('singlepost',$news->id)}}">{{$news->title}}</a></h2>
                  <span class="author mb-3 d-block">{{$news->user->name}}</span>
                </div>
                @endif
                @endforeach
              </div>
            </div>
          </div>

        </div> <!-- End .row -->
      </div>
    </section><!-- End Lifestyle Category Section -->

  </main><!-- End #main -->


@endsection
